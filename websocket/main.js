const cors = require('cors');
const morgan = require('morgan');
const express = require('express');
const expressWS = require('express-ws');

const PORT = 3000;

const app = express();
const ws = expressWS(app);

const chats = {}

app.use(cors());
app.use(morgan('tiny'));

app.get('/api/list',
	(req, resp) => {
		resp.status(200).json(Object.keys(chats));
	}
)

app.get('/api/list/:group',
	(req, resp) => {
		const group = req.params.group;
		if (!(group in chats))
			return resp.status(404).json({ message: `No such group: ${group}` });
		resp.status(200).json({
			groupName: group,
			participants: Object.keys(chats[group])
		})
	}
)

app.ws('/join/:group/:user',
	(ws, req) => {
		const group = req.params.group
		const user = req.params.user;

		console.info(`User ${user} joined ${group}`)

		if (!(group in chats))
			chats[group] = { }
		chats[group][user] = ws;

		ws.on('message', 
			(payload) => {
				const data = JSON.parse(payload);
				switch (data.msgType.toLowerCase()) {
					case 'message':
						console.info('message: ', data.message);
						const users = chats[group] || [];
						for (let u in users)
							users[u].send(`[${new Date()}] ${data.message}`)
						break;

					default: // assume 'leave'
						// Closes the connection
						chats[data.group][data.user].close();
						delete chats[data.group][data.user];
				}
			}
		)
	}
)

app.listen(PORT, 
	() => {
		console.info(`Application started on port ${PORT} at ${new Date()}`);
	}
)
